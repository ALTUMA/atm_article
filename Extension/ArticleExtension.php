<?php

namespace ATM\ArticleBundle\Extension;

use Doctrine\ORM\EntityManager;

class ArticleExtension  extends \Twig_Extension{

    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getArticles', array($this, 'getArticles')),
            new \Twig_SimpleFunction('getTotalArticles', array($this, 'getTotalArticles'))
        );
    }

    public function getArticles($maxResults = null){

        $ids = $this->em->getRepository('ATMArticleBundle:Article')->getArticlesIds($maxResults);

        $arrIds = array_map(function($a){
            return $a['id'];
        },$ids);

        if(count($arrIds) == 0){
            return false;
        }

        return $this->em->getRepository('ATMArticleBundle:Article')->getArticlesByIds($arrIds);
    }

    public function getTotalArticles(){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('COUNT(a.id) as totalArticles')
            ->from('ATMArticleBundle:Article','a')
            ->where(
                $qb->expr()->lte('a.publishDate','CURRENT_TIMESTAMP()')
            );

        $result = $qb->getQuery()->getArrayResult();

        return $result[0]['totalArticles'];
    }

    public function getName()
    {
        return 'atm_article_extension';
    }
}