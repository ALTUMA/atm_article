<?php

namespace ATM\ArticleBundle\Repository;

use Doctrine\ORM\EntityRepository;


class ArticleRepository extends EntityRepository
{
    public function getArticlesIds($maxResults = null){
        $qbIds = $this->getEntityManager()->createQueryBuilder();

        $qbIds
            ->select('partial a.{id}')
            ->from('ATMArticleBundle:Article','a')
            ->where(
                $qbIds->expr()->lte('a.publishDate','CURRENT_TIMESTAMP()')
            )
            ->orderBy('a.publishDate','DESC');

        $query = $qbIds->getQuery();

        if($maxResults){
            $query->setMaxResults($maxResults);
        }

        return $query->getArrayResult();
    }

    public function getArticlesByIds($articlesIds){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('a')
            ->addSelect('author')
            ->addSelect('c')
            ->addSelect('FIELD(a.id,'.implode(',',$articlesIds).') as HIDDEN sorting')
            ->from('ATMArticleBundle:Article','a')
            ->join('a.category','c')
            ->join('a.author','author')
            ->where(
                $qb->expr()->in('a.id',$articlesIds)
            )
            ->orderBy('sorting');

        return $qb->getQuery()->getArrayResult();
    }
}