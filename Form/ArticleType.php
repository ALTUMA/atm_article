<?php

namespace ATM\ArticleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class,array(
                'required' => true
            ))
            ->add('summary',TextareaType::class,array(
                'required' => true
            ))
            ->add('publishDate',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Init Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('body',TextareaType::class,array(
                'required' => true
            ))
            ->add('category', EntityType::class, array(
                'class' => $options['categoryNamespace'],
                'query_builder' => function($er) {
                    $qb = $er->createQueryBuilder('cat');
                    return $qb
                        ->orderBy('cat.name', 'ASC');
                },
                'choice_label' => 'name',
                'placeholder' => '',
                'empty_data'  => null,
                'required' => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'categoryNamespace' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'atmarticle_bundle_article_type';
    }
}
