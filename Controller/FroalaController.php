<?php

namespace ATM\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class FroalaController extends Controller{

    public function uploadImageAction($folder){
        $request = $this->get('request_stack')->getCurrentRequest();
        $config = $this->getParameter('atm_article_config');
        $mediaFolder = $config['media_folder'];

        $articlesImagesPath = $this->get('kernel')->getRootDir().'/../web/'.$mediaFolder.'/atm_articles_images';
        if(!is_dir($articlesImagesPath)){
            mkdir($articlesImagesPath);
        }

        $messageImagesFolder = $articlesImagesPath.'/'.$folder;
        if(!is_dir($messageImagesFolder)){
            mkdir($messageImagesFolder);
        }

        $file = $request->files->get('froala_image_file');
        $imageName = $file->getClientOriginalName();
        $file->move($messageImagesFolder,$imageName);

        $hwcdn = $this->get('xlabs_hwcdn');

        return new Response(json_encode(array('link'=>$hwcdn->getCDNResource(array('media_path' =>'/'.$mediaFolder.'/atm_articles_images/'.$folder.'/'.$imageName)))));
    }

    public function deleteImageAction($path){

        $imageFolder = $this->get('kernel')->getRootDir().'/../web'.urldecode($path);
        unlink($imageFolder);
        return new Response(json_encode('success'));
    }

}

